from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String, nullable=False)
    value = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Task %r>' % self.id

#@app.route('/')
#def home():
#    return redirect('/event')

@app.route('/', methods=['POST', 'GET'])
def event():
	if request.method == 'POST':
		ty = request.form['type']
		val = request.form['value']
		new_task = Todo(type=ty, value=val) # 'Todo' object has no attribute 'ty_type'

		try:
			db.session.add(new_task)
			db.session.commit()
			return redirect('/value')
		except:
			return "There was an issue."
	else:
		tasks = Todo.query.order_by(Todo.date_created).all()
		return render_template('index.html',tasks=tasks)

@app.route('/events', methods=['GET'])
def events():
	eve = Todo.query.order_by(Todo.date_created).all()
	return render_template('events.html', eve=eve)

@app.route('/value', methods=['GET'])
def value():
	eve = Todo.query.order_by(Todo.date_created).all()
	num = 0
	for i in eve:
		if i.type == 'INCREMENT':
			num = (num + int(i.value))
		else:
			num = (num - int(i.value))
	return str(num)

@app.route('/value/<int:t>')
def t_val(t):
	eve = Todo.query.order_by(Todo.date_created).all()
	a = t
	num = 0
	lis = []
	
	if a < len(eve):
		for i in range(0,a):
			lis.append(eve[i])
		for j in lis:
			if j.type == 'INCREMENT':
				num = num + int(j.value)
			else:
				num = num - int(j.value)
		return str(num)
	else:
		return redirect('/value')

	#if request.method=='POST':
	#	ty = request.form('type')
	#	val = request.form('value')
	#else:
	#	ty = request.args.get('type')
	#	val = request.args
	#	.get('value')
	#	print("type: ",ty)
	##	return render_template('value.html')

#@app.route('/events', methods=['GET'])
#def events():
#	eve = Todo.query.order_by(Todo.date_created).all()
#    return render_template('events.html')



if __name__=='__main__':
    app.debug=True
    app.run()

